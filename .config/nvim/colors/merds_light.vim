" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" Maintainer:	Marco Eduardo <meduardo.r@tutanota.com>
" Create: 22 May 2019
" Last Change:	03 Feb 2022

set background=light
highlight clear
if exists("syntax_on")
  syntax reset
endif

set t_Co=256

let g:colors_name = "merds_light"
let g:terminal_color_0 = '#ffffec'

hi Comment       ctermfg=36
hi Constant      ctermfg=4                        cterm=none    
hi Identifier    ctermfg=136     
hi Statement     ctermfg=245    ctermbg=NONE      cterm=bold    
hi PreProc       ctermfg=28     ctermbg=NONE      cterm=italic      
hi Function      ctermfg=252    ctermbg=NONE      cterm=bold   
hi String        ctermfg=124    ctermbg=NONE      cterm=NONE
hi Keyword       ctermfg=34     ctermbg=NONE      cterm=NONE    
hi Include       ctermfg=33     ctermbg=NONE      cterm=italic 
hi Delimiter     ctermfg=52     ctermbg=NONE      cterm=NONE
hi Type          ctermfg=33     
hi Special       ctermfg=88     
hi Error         ctermbg=9     
hi Todo          ctermfg=226    ctermbg=0         cterm=bold    
hi Directory     ctermfg=2     
hi StatusLine    ctermfg=16     ctermbg=156       cterm=none    
hi Normal        ctermfg=231
hi Search        ctermbg=3    
hi Operator      ctermfg=5    
hi Number        ctermfg=130   
hi StatuslineNC  ctermfg=NONE   ctermbg=191
hi LineNr        ctermfg=137    ctermbg=NONE     cterm=NONE
hi CursorLine    ctermfg=NONE   ctermbg=NONE      cterm=NONE
hi CursorColumn  ctermfg=NONE   ctermbg=193       
hi MatchParen    ctermfg=33     ctermbg=NONE     cterm=bold 
hi Title         ctermfg=4      ctermbg=NONE     cterm=bold
hi SpellBad      ctermfg=36     ctermbg=NONE     cterm=underline
hi SpellCap      ctermfg=130    ctermbg=NONE     cterm=underline
hi SpellLocal    ctermfg=4      ctermbg=NONE     cterm=underline
hi SpellRare     ctermfg=5      ctermbg=NONE     cterm=underline
hi DiffChange    ctermfg=NONE   ctermbg=254      cterm=NONE
hi DiffAdd       ctermfg=NONE   ctermbg=194      cterm=NONE
hi DiffText      ctermfg=NONE   ctermbg=230      cterm=NONE
hi DiffDelete    ctermfg=167    ctermbg=224      cterm=NONE

syn match    customHeader1     "^# "
syn match    customHeader2     "^## "
syn match    customHeader3     "^### "
syn match    customHeader4     "^#### "
syn match    customHeader5     "^##### "

highlight customHeader1 ctermfg=34
highlight customHeader2 ctermfg=32
highlight customHeader3 ctermfg=127
highlight customHeader4 ctermfg=45
highlight customHeader5 ctermfg=220
