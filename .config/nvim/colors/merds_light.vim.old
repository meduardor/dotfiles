" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" Name:         merds_light
" Maintainer:	Marco Eduardo <meduardo.r@protomail.com>
" Last Change:	24 May 2019

set background=light
highlight clear
let g:colors_name = "merds_light"

"set t_Co=256

let s:t_Co = exists('&t_Co') && !empty(&t_Co) && &t_Co > 1 ? &t_Co : 1

if (has('termguicolors') && &termguicolors) || has('guirunning')
    let g:terminal_ansi_colors = ['#424242', '#b8261e', '#3e8630', '#7f8f29','#2a8dc5', '#8888cf', '#6aa7a8', '#999957', '#a8a825', '#f2acaa', '#98ce8f', '#b6b79c', '#a6dcf8', '#d0d1f7', '#b0eced', '#87c647']
    if has('nvim')
        let g:terminal_color_0 =  '#424242' 
        let g:terminal_color_1 =  '#b8261e'
        let g:terminal_color_2 =  '#3e8630'
        let g:terminal_color_3 =  '#7f8f29'
        let g:terminal_color_4 =  '#2a8dc5'
        let g:terminal_color_5 =  '#8888cf'
        let g:terminal_color_6 =  '#6aa7a8'
        let g:terminal_color_7 =  '#999957'
        let g:terminal_color_8 =  '#a8a825'
        let g:terminal_color_9 =  '#f2acaa'
        let g:terminal_color_10 = '#98ce8f'
        let g:terminal_color_11 = '#b6b79c'
        let g:terminal_color_12 = '#a6dcf8'
        let g:terminal_color_13 = '#d0d1f7'
        let g:terminal_color_14 = '#b0eced'
        let g:terminal_color_15 = '#87c647'
    endif
    hi Comment       ctermfg=36
    hi Constant      ctermfg=4                       cterm=none    
    hi Identifier    ctermfg=136     
    hi Statement     ctermfg=240   ctermbg=NONE      cterm=bold    
    hi PreProc       ctermfg=40    
    hi Function      ctermfg=57    ctermbg=NONE      cterm=bold   
    hi String        ctermfg=75    ctermbg=NONE      cterm=bold
    hi Keyword       ctermfg=34    ctermbg=NONE      cterm=NONE    
    hi Include       ctermfg=33    ctermbg=NONE      cterm=italic 
    hi Delimiter     ctermfg=52    ctermbg=NONE      cterm=NONE
    hi MatchParen    ctermfg=9     ctermbg=109       cterm=NONE 
    hi Type          ctermfg=33     
    hi Special       ctermfg=88     
    hi Error         ctermbg=9     
    hi Todo          ctermfg=226   ctermbg=NONE      cterm=bold    
    hi Directory     ctermfg=2     
    hi StatusLine    ctermfg=16    ctermbg=156       cterm=none    
    hi Normal        ctermfg=59
    hi Search        ctermbg=3    
    hi Operator      ctermfg=5    
    hi Number        ctermfg=130   
    hi StatuslineNC  ctermfg=NONE   ctermbg=191
    hi LineNr        ctermfg=137    ctermbg=NONE     cterm=NONE
    hi CursorLine    ctermfg=NONE   ctermbg=191      cterm=NONE
    hi CursorColumn  ctermfg=NONE   ctermbg=193       
    hi Title         ctermfg=226    ctermbg=NONE     cterm=bold
    hi SpellBad      ctermfg=36     ctermbg=NONE     cterm=bold
    hi SpellCap      ctermfg=130    ctermbg=0        cterm=bold
    hi SpellLocal    ctermfg=4      ctermbg=NONE     cterm=underline
    hi SpellRare     ctermfg=5      ctermbg=NONE     cterm=underline
    hi DiffChange    ctermfg=NONE   ctermbg=254      cterm=NONE
    hi DiffAdd       ctermfg=NONE   ctermbg=194      cterm=NONE
    hi DiffText      ctermfg=NONE   ctermbg=230      cterm=NONE
    hi DiffDelete    ctermfg=167    ctermbg=224      cterm=NONE
    unlet s:t_Co
    finish
endif
