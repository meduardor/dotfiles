;;; Commentary: Package Manager (straight-use-package)
;;; package --- Summary

(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

;; Use-Package
(straight-use-package 'use-package)
(setq straight-use-package-by-default t)


(setq native-comp-async-report-warnings-errors 'silent)

;; Minimal Config
(set-face-attribute 'default nil
        :family "Iosevka Nerd Font"
        :weight 'regular
        :width  'normal
        :height 120)

(defconst private-dir (expand-file-name "private" user-emacs-directory))
(defconst temp-dir (format "%s/cache" private-dir)
  "Hostname-based elisp temp directories.")

;; Emacs Custom
(setq confirm-kill-emacs                  'y-or-n-p
      confirm-nonexistent-file-or-buffer  t
      save-interprogram-paste-before-kill t
      mouse-yank-at-point                 t
      require-final-newline               t
      visible-bell                        nil
      ring-bell-function                  'ignore
      custom-file                         "~/.emacs.d/.custom.el"
      minibuffer-prompt-properties        '(read-only t point-entered minibuffer-avoid-prompt face minibuffer-prompt)
      cursor-in-non-selected-windows      nil
      highlight-nonselected-windows       nil
      exec-path                           (append exec-path '("/usr/local/bin"))
      indent-tabs-mode                    nil
      inhibit-startup-message             t
      fringes-outside-margins             t
      select-enable-clipboard             t)

;; Bookmarks
(setq bookmark-save-flag    t
      bookmark-default-file (concat temp-dir "/bookmark"))

;; Backups enable
(setq history-length                 1000
      backup-inhibited               nil
      make-backup-files              t
      auto-save-default              t
      auto-save-list-file-name       (concat temp-dir "/autosave")
      create-lockfiles               nil
      backup-directory-alist         `((".*" . ,(concat temp-dir "/backup")))
      auto-save-file-name-transforms `((".*" ,(concat temp-dir "/auto-save-list") t)))

(unless (file-exists-p (concat temp-dir "/auto-save-list"))
  (make-directory (concat temp-dir "/auto-save-list") :parents))

(fset 'yes-or-no-p 'y-or-n-p)
(global-auto-revert-mode t)

;; Start Initial Windows
(defun mydefault-window-setup ()
  "Called by Emacs startup-hook to set up my initial window configuration."
  (split-window-right)
  (other-window 1)
  (find-file "~/.org/Tarefas.org")
  (other-window 1))
(add-hook 'emacs-startup-hook #'mydefault-window-setup)

;; Disable Tool Bar
(menu-bar-mode -1)
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'scroll-bar-mode)
  (scroll-bar-mode -1))

(show-paren-mode 1)

;; Columns / Lines Number
(column-number-mode)
(global-display-line-numbers-mode t)
(setq display-line-numbers-type 'relative)

;; Disable Line Numbers for some modes
(dolist (mode '(org-mode-hook
                term-mode-hook
                eshell-mode-hook
		markdown-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; Spell
(global-set-key (kbd "<f8>") 'ispell-word)
(global-set-key (kbd "C-S-<f8>") 'flyspell-mode)
(global-set-key (kbd "C-M-<f8>") 'flyspell-buffer)
(global-set-key (kbd "C-<f8>") 'flyspell-check-previous-highlighted-word)

(defun flyspell-check-next-highlighted-word ()
  "Custom function to spell next highlighted  word"
  (interactive)
  (flyspell-goto-next-error)
  (ispell-word))

(global-set-key (kbd "M-<f8>") 'flyspell-check-next-highlighted-word)

;; Move Line and Region

(add-to-list 'load-path (concat user-emacs-directory "after"))
;; (require 'line-region-mode)
;; (require 'symbols)

;; Packages Install

(use-package command-log-mode
  :ensure t
  :init)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; UI Packages

;; Rainbow Delimiters
(use-package rainbow-delimiters
  :init
  (rainbow-delimiters-mode 1)
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))


;; Auto-Pair
(use-package smartparens
  :config
  (smartparens-global-mode 1))

;; Parinfer
(use-package parinfer
  :ensure t
  :bind
  (("C-," . parinfer-toggle-mode))
  :init
  (progn
    (setq parinfer-extensions
    '(defaults
       pretty-parens
       evil
       lispy
       paredit
       smart-tab
       smart-yank))
    (add-hook 'emacs-lisp-mode  #'parinfer-mode)
    (add-hook 'common-lisp-mode #'parinfer-mode)
    (add-hook 'scheme-mode      #'parinfer-mode)
    (add-hook 'lisp-mode        #'parinfer-mode)
    (add-hook 'racket-mode      #'parinfer-mode)))

;;Windowmove-mode
(use-package windmove
  :bind (("C-x <up>"    . windmove-up)
         ("C-x <down>"  . windmove-down)
         ("C-x <left>"  . windmove-left)
         ("C-x <right>" . windmove-right)))

;; Other Windows Move
(global-set-key (kbd "<C-tab>") 'other-window)
(global-set-key (kbd "C-~") 'next-buffer)
(global-set-key (kbd "C-´") 'previous-buffer)

;; Which Key
(use-package which-key
  :init
  (which-key-mode)
  :diminish
  (which-key-mode)
  :config
  (setq which-key-idle-delay 0.3))

;; Buffer
(use-package bufler
  :disabled
  :bind
  ("C-x b" . #'bufler-switch-buffer)
  ("C-x B" . #'counsel-switch-buffer))

(defun revert-to-two-windows ()
  "Delete all other windows and split in into two."
  (interactive)
  (delete-other-windows)
  (split-window-right))

(bind-key "C-x 1" #'revert-to-two-windows)
(bind-key "C-x !" #'delete-other-windows)

;; ;; Kill Buffer
;; (defun kill-this-buffer ()
;;   "Kill the current buffer."
;;   (interactive)
;;   (kill-buffer nil))

;; (bind-key "C-x k" . #'kill-this-buffer)
;; (bind-key "C-x K" . #'kill-buffer)

;; Ivy
(use-package ivy
  :diminish
  :custom
  (ivy-height 30)
  (ivy-use-virtual-buffers t)
  (ivy-use-selectable-prompt t)
  :config
  (ivy-mode 1)
  :bind
  ("C-c C-r" . #'ivy-resume)
  ("C-c s"   . #'swiper-thing-at-point)
  ("C-s"     . #'swiper))

(use-package ivy-rich
  :after counsel-mode
  :custom
  (ivy-virtual-addreviate 'full)
  (ivy-rich-switch-buffer-align-virtual-buffer nil)
  (ivy-rich-path-style 'full)
  :config
  (setcdr (assq t ivy-format-functions-alist) #'ivy-format-function-line)
  (ivy-rich-mode))

(use-package counsel
  :init
  (counsel-mode 1)

  :bind (("C-c ;" . #'counsel-M-x)
         ("C-c U" . #'counsel-unicode-char)
         ("C-c i" . #'counsel-imenu)
         ("C-x f" . #'counsel-find-file)
         ("C-c y" . #'counsel-yank-pop)
         ("C-c r" . #'counsel-recentf)
         ("C-c v" . #'counsel-switch-buffer-other-window)
         ("C-h h" . #'counsel-command-history)
         ("C-x C-f" . #'counsel-find-file)
         :map ivy-minibuffer-map
         ("C-r" . counsel-minibuffer-history))
  :diminish)


(use-package ivy-posframe
  :init
  (ivy-posframe-mode 1)
  :config
  (setq ivy-posframe-height-alist
        '((swiper . 10)
          (counsel-find-file . 10)
          (counsel-M-x . 10)
          (t . 40)))
  (setq ivy-posframe-display-functions-alist
        '((swiper . ivy-posframe-display-at-point)
          (complete-symbol . ivy-posframe-display-at-point)
          (counsel-find-file . ivy-posframe-display-at-frame-center)
          (counsel-M-x . ivy-posframe-display-at-frame-bottom-left)
          (t . ivy-posframe-display))))


(use-package all-the-icons
  :ensure t)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Themes
;; Doom Themes
;; (use-package doom-themes
;;              :ensure t
;;              :config
;;              (setq doom-themes-enable-bold t
;;                    doom-themes-enable-italic t)
;;              (load-theme 'doom-one t)
;;              (doom-themes-visual-bell-config)
;;              (doom-themes-org-config))

;;; Themes Berrys
;; (use-package berrys-theme
;;   :config
;;   (setq-default line-spacing 2)
;;   ;; Muda o cursor do emacs
;;   (setq-default cursor-type '(bar . 2))
;;   (load-theme 'berrys t))

;;; Themes Leuven
(use-package leuven-theme
  :config
  (setq leuven-scale-org-agenda-structure nil
        leuven-scale-outline-headlines nil
        leuven-scale-volatile-highlight nil)
  (load-theme 'leuven-dark t))

;;; Theme Modus
;; (use-package modus-themes
;;   :ensure                         ; omit this to use the built-in themes
;;   :init
;;   ;; Add all your customizations prior to loading the themes
;;   (setq modus-themes-italic-constructs t
;;         modus-themes-bold-constructs t
;;         modus-themes-region '(bg-only no-extend)
;; 	modus-themes-syntax '(alt-syntax green-strings yellow-comments))
;;   ;; Load the theme files before enabling a theme (else you get an error).
;;   (modus-themes-load-themes)
;;   :config
;;   ;; Load the theme of your choice:
;;   (modus-themes-load-operandi) ;; OR (modus-themes-load-vivendi)
;;   :bind ("<f5>" . modus-themes-toggle))


;;; Theme Bespoke(elegant/nano themes equal)
;; (use-package bespoke-themes
;;   :straight (:host github :repo "mclear-tools/bespoke-themes" :branch "main")
;;   :config
;;   ;; Set evil cursor colors
;;   (setq bespoke-set-evil-cursors t)
;;   ;; Set use of italics
;;   (setq bespoke-set-italic-comments t
;;         bespoke-set-italic-keywords t)
;;   ;; Set variable pitch
;;   (setq bespoke-set-variable-pitch t)
;;   ;; Set initial theme variant
;;   (setq bespoke-set-theme 'dark)
;;   ;; Load theme
;;   (load-theme 'bespoke t))

;; Windows Themes (Config)

;; Make a clean & minimalist frame
(use-package frame
  :straight (:type built-in)
  :config
  (setq-default default-frame-alist
                (append (list
			 '(font . "Iosevka Nerd Font:style=regular:size=15") ;; NOTE: substitute whatever font you prefer here
			 '(internal-border-width . 20)
			 '(left-fringe    . 0)
			 '(right-fringe   . 0)
			 '(tool-bar-lines . 0)
			 '(menu-bar-lines . 0)
			 '(vertical-scroll-bars . nil)))))
  (setq-default window-resize-pixelwise t)
  (setq-default frame-resize-pixelwise t)

;; Dim inactive windows
(use-package dimmer
  :straight (:host github :repo "gonewest818/dimmer.el")
  :hook (after-init . dimmer-mode)
  :config
  (setq dimmer-fraction 0.5)
  (setq dimmer-adjustment-mode :foreground)
  (setq dimmer-use-colorspace :rgb)
  (setq dimmer-watch-frame-focus-events nil)
  (dimmer-configure-which-key)
  (dimmer-configure-magit)
  (dimmer-configure-posframe))

;;; Mode-line (Themes)
;; (use-package bespoke-modeline
;;   :straight (:type git :host github :repo "mclear-tools/bespoke-modeline") 
;;   :init
;;   ;; Set header line
;;   (setq bespoke-modeline-position 'top)
;;   ;; Set mode-line height
;;   (setq bespoke-modeline-size 2)
;;   ;; Show diff lines in mode-line
;;   (setq bespoke-modeline-git-diff-mode-line t)
;;   ;; Set mode-line cleaner
;;   (setq bespoke-modeline-cleaner t)
;;   ;; Use mode-line visual bell
;;   (setq bespoke-modeline-visual-bell t)
;;   ;; Set vc symbol
;;   (setq  bespoke-modeline-vc-symbol "G:")
;;   :config
;;   (bespoke-modeline-mode))

;;; My Mode-line 

(use-package doom-modeline
  :ensure t
  :hook (after-init . doom-modeline-mode)
  :config
  (setq doom-modeline-height 25)
  (setq doom-modeline-bar-width 4)
  (setq doom-modeline-hud nil)
  (setq doom-modeline-window-width-limit fill-column)
  (setq doom-modeline-project-detection 'auto)
  (setq doom-modeline-buffer-file-name-style 'auto)
  (setq doom-modeline-icon (display-graphic-p))
  (setq doom-modeline-major-mode-icon t)
  (setq doom-modeline-major-mode-color-icon t)
  (setq doom-modeline-buffer-state-icon t)
  (setq doom-modeline-buffer-modification-icon t)
  (setq doom-modeline-unicode-fallback nil)
  (setq doom-modeline-minor-modes nil)
  (setq doom-modeline-enable-word-count nil)
  (setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))
  (setq doom-modeline-buffer-encoding t)
  (setq doom-modeline-indent-info nil)
  (setq doom-modeline-checker-simple-format t)
  (setq doom-modeline-number-limit 99)
  (setq doom-modeline-vcs-max-length 12)
  (setq doom-modeline-workspace-name t)
  (setq doom-modeline-persp-name t)
  (setq doom-modeline-display-default-persp-name nil)
  (setq doom-modeline-persp-icon t)
  (setq doom-modeline-lsp t)
  (setq doom-modeline-github nil)
  (setq doom-modeline-github-interval (* 30 60))
  (setq doom-modeline-modal-icon t)
  (setq doom-modeline-env-version t)
  (setq doom-modeline-env-enable-python t)
  (setq doom-modeline-env-enable-perl t)
  (setq doom-modeline-env-enable-go t)
  (setq doom-modeline-env-enable-rust t)
  (setq doom-modeline-env-python-executable "python") ; or `python-shell-interpreter'
  (setq doom-modeline-env-perl-executable "perl")
  (setq doom-modeline-env-go-executable "go")
  (setq doom-modeline-env-rust-executable "rustc")
  (setq doom-modeline-env-load-string "...")
  (setq doom-modeline-before-update-env-hook nil)
  (setq doom-modeline-after-update-env-hook nil))

(set-face-attribute 'mode-line nil :family "Fira Mono Nerd Font" :height 0.9)
(set-face-attribute 'mode-line-inactive nil :family "Fira Mono Nerd Font" :height 0.9)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; KeyBindigs Packages / Config

;; Iedit
(use-package iedit
  :ensure t)

(defun iedit-dwin (agr)
  (interactive "P")
  (if arg
      (ideit-mode)
    (save-excursion
      (save-restriction
        (widen)
        (if ideit-mode
            (ideit-done)
          (narrow-to-defun)
          (ideit-start (current-word) (point-min) (point-max)))))))

(global-set-key (kbd "C-;") 'iedit-dwin)

;; General
(use-package general
  :config
  (general-evil-setup t)
  
  (general-create-definer rune/leader-keys
        :keymaps '(normal insert visual emacs)
        :prefix "SPC"
        :global-prefix "C-SPC"))

(rune/leader-keys
  "t" '(:ignore t :which-key "toggles")
  "tt" '(consuel-load-theme :which-key "choose theme"))

;; Becoming Evil (evil-mode/evil-collection)

(defun rune/evil-hook ()
  (dolist (mode '(custom-mode
                  eshell-mode
                  git-rebase-mode
                  erc-mode
                  term-mode))
    (add-to-list 'evil-emacs-state-modes mode)))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-jump nil)
  :hook (evil-mode . rune/evil-hook)
  :config
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)
  ;; Use visual line motions
  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)
  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :config
  (evil-collection-init))

;; Hydra

(use-package hydra)

(defhydra hydra-text-scale (:timeout 4)
  "scale text"
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(rune/leader-keys
  "ts" '(hydra-text-scale/body :which-key "scale text"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Work and Projects

;;Projectile
(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Projetos/")
    (setq projectile-project-search-path '("~/Projetos/")))
  (setq projectile-switch-project-action #'projectile-dired));

;; Consuel-Projectile
(use-package counsel-projectile
  :bind (("C-c f" . #'counsel-projectile)
         ("C-c F" . #'counsel-projectile-switch-project)))
;; Magit

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package evil-magit
  :after magit)

(use-package forge
  :after magit)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Org Mode

(defun dw/org-mode-setup ()
  "Funções predefinidas para o orgmode."
  (org-indent-mode)
  (variable-pitch-mode 0)
  (auto-fill-mode 1)
  (visual-line-mode 1)
  (setq truncate-lines nil)
  (setq-default fill-column 3000)
  (setq evil-auto-indent nil))

(use-package org
  :hook (org-mode . dw/org-mode-setup)
  :config
  (setq org-ellipsis " ▾")
  org-hide-emphasis-markers t
  (setq org-directory "~/.org"
        org-default-notes-file (concat org-directory "~/.org"))
  (setq org-agenda-files (list "~/.org/Tarefas.org"
                                "~/.org/Aniversario.org"
                                "~/.org/Habitos.org"))
  :bind
  ("C-c L" . org-stored-links)
  ("C-c a" . org-agenda))

;;; Funcition Click enter Link (Org-Mode)
;; If you press `RET` on a link inside a table it doesn't work as expected.
(defun org-clicky()
  (interactive)
  (if (eq 'org-link (get-text-property (point) 'face))
      (org-open-at-point)
    (org-return)))
   
(add-hook 'org-mode-hook
          (lambda ()
            (local-set-key (kbd "RET") 'org-clicky)))


;; Org-mode Bullets (*, **, ***, hierarquia dos títulos)
(use-package org-bullets
  :after org
  :hook
  (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

;;Replace list hyphen with dot
(font-lock-add-keywords 'org-mode
      '(("^ *\\([-]\\) "
         (0 (prog1 () (compose-region (match-beginning 1) (match-end 1) "•"))))))


;; Make sure org-indent face is available
(require 'org-indent)

;; Ensure that anything that should be fixed-pitch in Org files appears that way
(set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
(set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
(set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
(set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
(set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)

;; If you only want to see the agenda for today
;; (setq org-agenda-span 'day)

(setq org-agenda-start-with-log-mode t)
(setq org-log-done 'time)
(setq org-log-into-drawer t)

;; Agenda Org (TODOS)

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d!)")
        (sequence "BACKLOG(b)" "PLAN(p)" "READY(r)" "ACTIVE(a)" "REVIEW(v)" "WAIT(w@/!)" "HOLD(h)" "|" "COMPLETED(c)" "CANC(k@)")))

;; Agenda Org (VIEWS)

;; Configure custom agenda views
(setq org-agenda-custom-commands
  '(("d" "Dashboard"
     ((agenda "" ((org-deadline-warning-days 7)))
      (todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))
      (tags-todo "agenda/ACTIVE" ((org-agenda-overriding-header "Active Projects")))))

    ("n" "Next Tasks"
     ((todo "NEXT"
        ((org-agenda-overriding-header "Next Tasks")))))


    ("W" "Work Tasks" tags-todo "+work")
  ;; Low-effort next actions
    ("e" tags-todo "+TODO=\"NEXT\"+Effort<15&+Effort>0"
     ((org-agenda-overriding-header "Low Effort Tasks")
      (org-agenda-max-todos 20)
      (org-agenda-files org-agenda-files)))

    ("w" "Workflow Status"
     ((todo "WAIT"
            ((org-agenda-overriding-header "Waiting on External")
             (org-agenda-files org-agenda-files)))
      (todo "REVIEW"
            ((org-agenda-overriding-header "In Review")
             (org-agenda-files org-agenda-files)))
      (todo "PLAN"
            ((org-agenda-overriding-header "In Planning")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "BACKLOG"
            ((org-agenda-overriding-header "Project Backlog")
             (org-agenda-todo-list-sublevels nil)
             (org-agenda-files org-agenda-files)))
      (todo "READY"
            ((org-agenda-overriding-header "Ready for Work")
             (org-agenda-files org-agenda-files)))
      (todo "ACTIVE"
            ((org-agenda-overriding-header "Active Projects")
             (org-agenda-files org-agenda-files)))
      (todo "COMPLETED"
            ((org-agenda-overriding-header "Completed Projects")
             (org-agenda-files org-agenda-files)))
      (todo "CANC"
            ((org-agenda-overriding-header "Cancelled Projects")
             (org-agenda-files org-agenda-files)))))))

;; Refiling

(setq org-refile-targets
      '(("Archive.org" :maxlevel . 1)))

;; Save Org buffers after refiling!
(advice-add 'org-refile :after 'org-save-all-org-buffers)

;; Capture Templates

(defun dw/read-file-as-string (path)
  (with-temp-buffer
    (insert-file-contents path)
    (buffer-string)))

(setq org-capture-templates
      `(("t" "Tasks / Projetos")
        ("tt" "Task" entry (file+olp "~/.org/Orgfiles/Tasks.org" "Inbox")
         "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)
        ("ts" "Clocked Entry Subtask" entry (clock)
         "* TODO %?\n  %U\n  %a\n  %i" :empty-lines 1)

        ("j" "Journal Entries")
        ("jj" "Journal" entry
         (file+olp+datetree "~/.org/Orgfiles/Journal.org")
         "\n* %<%I:%M %p> - Journal :journal:\n\n%?\n\n"
         ;; ,(dw/read-file-as-string "~/Notes/Templates/Daily.org")
         :clock-in :clock-resume
         :empty-lines 1)
        ("jm" "Meeting" entry
         (file+olp+datetree "~/.org/Orgfiles/Journal.org")
         "* %<%I:%M %p> - %a :meetings:\n\n%?\n\n"
         :clock-in :clock-resume
         :empty-lines 1)
        ("m" "Metrics Capture")
        ("mw" "Weight" table-line (file+headline "~/.org/OrgFiles/Metrics.org" "Weight")
         "| %U | %^{Weight} | %^{Notes} |" :kill-buffer t)))

(define-key global-map (kbd "C-c j")
  (lambda () (interactive) (org-capture nil "j")))

;;; Habitos

(require 'org-habit)
(add-to-list 'org-modules 'org-habit)
(setq org-habit-graph-column 60)


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; ORG_ROAM (Método de Estudo Zettelkasten)

(use-package org-roam
  :ensure t
  :init
  (setq org-roam-v2-ack t)
  :custom
  (org-roam-directory (file-truename "~/.org/roam"))
  (org-roam-completion-everywhere t)
  (org-roam-dailies-capture-templates
   '(("d" "default" entry "* %<%I:%M %p>: %?"
      :if-new (file+head "%<%Y-%m-%d>.org" "#+title: %<%Y-%m-%d>\n"))))
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         ("C-c n c" . org-roam-capture)
         ("C-c n g" . org-roam-graph))
   (:map org-mode-map
         ("C-M-i" . completion-at-point))
   (:map org-roam-dailies-map
         ("Y" . org-roam-dailies-capture-yesterday)
         ("T" . org-roam-dailies-capture-tomorrow))
  :bind-keymap
  ("C-c n d" . org-roam-dailies-map)
  :config
  (require 'org-roam-dailies)
  (org-roam-db-autosync-mode)
  ;; If using org-roam-protocol
  (require 'org-roam-protocol))

;;Config deletes Dailies
;("C-c n j" . org-roam-dailies-capture-today)

;; Org-Roam-ui
(use-package org-roam-ui
  :straight
    (:host github :repo "org-roam/org-roam-ui" :branch "main" :files ("*.el" "out"))
    :after org-roam
;;         normally we'd recommend hooking orui after org-roam, but since org-roam does not have
;;         a hookable mode anymore, you're advised to pick something yourself
;;         if you don't care about startup time, use
;;  :hook (after-init . org-roam-ui-mode)
    :config
    (setq org-roam-ui-sync-theme t
          org-roam-ui-follow t
          org-roam-ui-update-on-save t
          org-roam-ui-open-on-start t))

;;; Org-Roam-bibitex
(use-package org-roam-bibtex
  :after org-roam
  :load-path "~/.org/org_roam_bibtex" ; Modify with your own path where you cloned the repository
  :config
  (require 'org-ref)) ; optional: if Org Ref is not loaded anywhere else, load it here

;;;;;; Markdown-mode
(use-package markdown-mode
  :ensure t
  :hook (markdown-mode . dw/org-mode-setup)
  :commands (markdown-mode gfm-mode)
  :mode (("README\\.md\\'" . gfm-mode))
  :init (setq markdown-command
              (concat
       "/usr/bin/pandoc"
       " --from=markdown --to=html"
       " --standalone --mathjax --highlight-style=pygments")))

(setq markdown-asymmetric-header t)
(setq markdown-unordered-list-item-prefix "*   ")
(setq markdown-italic-underscore t)
(setq markdown-bold-underscore nil)
(setq markdown-spaces-after-code-fence 0)
(setq markdown-footnote-location 'subtree)
(setq markdown-enable-wiki-links t)
(setq markdown-link-space-sub-char "-")
(setq markdown-wiki-link-fontify-missing t)
(setq markdown-wiki-link-fontify-missing t)
(setq markdown-split-window-direction 'right)

;; Use visual-line-mode in gfm-mode
(defun my-gfm-mode-hook ()
  (visual-line-mode 1))
(add-hook 'gfm-mode-hook 'my-gfm-mode-hook)


;;; Mode Zen (Writer)

;; Writeroom Mode
(defun toby/toggle-minor-mode (mode)
  :docstring "Change mode default to writeroom,`mode` -> recebe argumentos que serão modificados."
  (if (symbol-value mode) (funcall (symbol-function mode) 0)))  

(defun toby/writeroom-mode-hook ()
  "Modificação do atributos da tela para o uso do writeroom."
  (toby/toggle-minor-mode 'display-line-numbers-mode)
  (toby/toggle-minor-mode 'hl-line-mode)
  ;;(toby/toggle-minor-mode 'visual-line-mode)
  (toby/toggle-minor-mode 'company-mode)
  (toby/toggle-minor-mode 'focus-mode))
(add-hook 'writeroom-mode-hook #'toby/writeroom-mode-hook)

(use-package writeroom-mode
  :init
  (setq writeroom-width 0.55)
  :bind ("C-c w" . 'writeroom-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Coding

;; Lsp-Mode
;;
;;
(use-package lsp-mode
  :commands (lsp lsp-execute-code-action)
  :init
  (setq lsp-keymap-prefix "C-c l")  ;; Or 'C-l', 's-l'
  :hook((go-mode . lsp-deferred)
        (clojure-mode . lsp-mode)
        ;; (lsp-mode . lsp-diagnostic-modeline-mode)
        (lsp-mode . lsp-ui-mode)
	(lsp-mode . lsp-enable-which-key-integration))
  :bind ("C-c C-c" . #'lsp-execute-code-action)
  :config
  (setq lsp-modeline-diagnostics-mode t
        lsp-modeline-code-actions-enable t
        lsp-enable-symbol-highlighting t
        lsp-lens-enable t
        lsp-ui-sideline-show-code-actions t
        lsp-ui-sideline-enable t)
  :custom
  (lsp-print-performance t)
  ;;(lsp-diagnostics-modeline-mode-scope :project)
  (lsp-log-io t)
  (lsp-file-watch-threshold 5000)
  (lsp-enable-file-watchers nil)
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  ;; enable / disable the hints as you prefer:
  (lsp-rust-analyzer-server-display-inlay-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-enable "skip_trivial")
  (lsp-rust-analyzer-display-chaining-hints t)
  (lsp-rust-analyzer-display-lifetime-elision-hints-use-parameter-names nil)
  (lsp-rust-analyzer-display-closure-return-type-hints t)
  (lsp-rust-analyzer-display-parameter-hints nil)
  (lsp-rust-analyzer-display-reborrow-hints nil))

;; Lsp-UI

(use-package lsp-ui
  :ensure
  :commands lsp-ui-mode
  :custom
  (lsp-ui-doc-delay 0.75)
  (lsp-ui-doc-max-height 200)
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil)
  :hook (lsp-mode . lsp-ui-mode)
  :after lsp-mode)

(setq lsp-ui-doc-position 'bottom)

;; Sideline(lsp-ui)

(setq lsp-ui-sideline-enable nil)
(setq lsp-ui-sideline-show-hover nil)

;; treemacs (lsp)
(use-package lsp-treemacs
  :after lsp)

;; Ivy (lsp)

(use-package lsp-ivy
  :after (ivy lsp-mode))

(use-package company-lsp
  :custom (company-lsp-enable-snippet t)
  :config
  (push 'company-lsp company-backends)
  :after (company lsp-mode))

;; Completions (Company-mode)
(use-package company
  :after lsp-mode
  :hook (prog-mode . company-mode)
  :bind (:map company-active-map)
        ("<tab>" . company-complete-selection)
  (:map lsp-mode-map)
  ("<tab>" . company-indent-or-complete-common)
  :custom
  (company-minimum-prefix-length 1)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))


;; Commenting lines
(use-package evil-nerd-commenter
    :bind ("M-/" . evilnc-comment-or-uncomment-lines))
(put 'scroll-left 'disabled nil)
(global-set-key (kbd "M-;") 'evilnc-copy-to-line)
(global-set-key (kbd "C-c l") 'evilnc-comment-or-uncomment-region)
(global-set-key (kbd "C-c c") 'evilnc-copy-and-comment-lines)
(global-set-key (kbd "C-c p") 'evilnc-comment-or-uncomment-paragraphs)

;;; EmacsTree-sitter
(use-package tree-sitter
  :init (global-tree-sitter-mode)
  :hook ((js-mode . tree-sitter-hl-mode))
   (go-mode . tree-sitter-hl-mode))
(add-hook 'tree-sitter-after-on-hook #'tree-sitter-hl-mode)
(use-package tree-sitter-langs)

;; Syntax check(flycheckers)
(use-package flycheck
  :after org
  :hook
  (org-src-mode . disable-flycheck-for-elisp)
  :custom
  (flycheck-emacs-lisp-initialize-packages t)
  (flycheck-display-errors-delay 0.1)
  :config
  (global-flycheck-mode)
  (flycheck-set-indication-mode 'left-margin)

  (defun disable-flycheck-for-elisp ()
    (setq-local flycheck-disabled-checkers '(emacs-lisp-checkdoc)))
  (add-to-list 'flycheck-checkers 'proselint))

(use-package flycheck-inline
  :config (global-flycheck-inline-mode))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; Language(coding)

(add-hook 'prog-mode-hook #'prettify-symbols-mode)

;; Nix-mode
(use-package nix-mode
  :mode "\\.nix\\'"
  :hook ('nix-mode-hook company-idle-delay t))

(use-package nix-buffer
  :commands nix-buffer)

;; Nim_Lang

(defun merds/init-nim-mode ()
  "Local init function for `nim-mode`."
  (local-set-key (kbd "M->") 'nim-indent-shift-right)
  (local-set-key (kbd "M-<") 'nim-indent-shift-left)
  (when (string-match "/\.nimble/" (or (buffer-file-name) "")) (read-only-mode))
  (nimsuggest-mode 1)
  (flycheck-mode 1)
  (auto-fill-mode 0)
  (electric-indent-local-mode 0))

(use-package nim-mode
  :ensure t
  :hook ((nim-mode . lsp-mode)
         (nim-mode . merds/init-nim-mode))
  :config
  (setq nimsuggest-path "~/.nimble/bin/nimsuggest"))


;;Inim-mode
;; (straight-use-package
;;  '(inim
;;    :type git
;;    :host github
;;    :repo "serialdev/inim-mode"
;;    :config
;;    (add-hook inim-mode-hook #'evcxr-minor-mode)
;; ))

;; CC-mode (lang C/Cpp)

(use-package cc-mode
  :config
  (setq c-default-style "linux")
  (setq gdb-many-windows t
        gdb-show-main t))

(use-package ede
  :config
  ;; Enable EDE only in C/C++
  (global-ede-mode))

(use-package ggtags
  :config
  (ggtags-mode 1)
  (add-hook 'c-mode-common-hook
      (lambda ()
        (when (derived-mode-p 'c-mode 'c++-mode 'java-mode 'asm-mode)))
    (ggtags-mode 1))

  (dolist (map (list ggtags-mode-map))
    (define-key map (kbd "C-c g s") 'ggtags-find-other-symbol)
    (define-key map (kbd "C-c g h") 'ggtags-view-tag-history)
    (define-key map (kbd "C-c g r") 'ggtags-find-reference)
    (define-key map (kbd "C-c g f") 'ggtags-find-file)
    (define-key map (kbd "C-c g c") 'ggtags-create-tags)
    (define-key map (kbd "C-c g u") 'ggtags-update-tags)
    (define-key map (kbd "M-.")     'ggtags-find-tag-dwim)
    (define-key map (kbd "M-,")     'pop-tag-mark)
    (define-key map (kbd "C-c <")   'ggtags-prev-mark)
    (define-key map (kbd "C-c >")   'ggtags-next-mark)))

(use-package cc-mode
  :init
  (define-key c-mode-map  [(tab)] 'company-complete)
  (define-key c++-mode-map  [(tab)] 'company-complete))


(defun alexott/cedet-hook ()
   "Semantica do c++."
   (local-set-key (kbd "C-c C-j") 'semantic-ia-fast-jump)
   (local-set-key (kbd "C-c C-s") 'semantic-ia-show-summary))

;; hs-minor-mode for folding source code
(add-hook 'c-mode-common-hook 'hs-minor-mode)
(add-hook 'c-mode-common-hook 'alexott/cedet-hook)
(add-hook 'c-mode-hook 'alexott/cedet-hook)
(add-hook 'c++-mode-hook 'alexott/cedet-hook)

(which-key-mode)
(add-hook 'c-mode-hook 'lsp)
(add-hook 'c++-mode-hook 'lsp)

(setq gc-cons-threshold (* 100 1024 1024)
      read-process-output-max (* 1024 1024)
      treemacs-space-between-root-nodes nil
      company-idle-delay 0.0
      company-minimum-prefix-length 1
      lsp-idle-delay 0.1)  ;; clangd is fast

(with-eval-after-load 'lsp-mode
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration))
;; (require 'dap-cpptools)
;; (yas-global-mode)


;; RUST_MODE (Lang Rust)

;; (use-package rust-mode
;;   :config
;;   (add-hook 'rust-mode-hook
;; 	    (lambda () (setq indent-tabs-mode nil)))
;;   (setq rust-format-on-save t)
;;   (add-hook 'rust-mode-hook #'lsp-mode))

(defun merds/rustic-mode-on ()
  "Save Buffers That Are Not File Visiting."
  (when buffer-file-name
    (setq-local buffer-save-without-query t)))


(use-package rustic
  :ensure
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-error-list-buffer)
              ("C-c C-c a" . lsp-auto-execute-action)
              ("C-c C-c r" . lsp-rename-use-prepare)
              ("C-c C-c q" . lsp-restart)
              ;; ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-macro-expansion-method))
  :config
  ;; (push 'rustic-cargo-clippy flycheck-checkers)
  (setq lsp-eldoc-hook nil)
  (setq lsp-enable-symbol-highlighting nil)
  (setq lsp-signature-auto-activate nil)
  (setq rustic-lsp-server 'rls)
  (setq rustic-lsp-client 'elgot)
  (setq rustic-analyzer-command '("~/.cargo/bin/rust-analyzer"))
  (setq rustic-format-on-save t)
  (setq rustic-rustfmt-args "+nightly")
  (setq rustic-rustfmt-config-alist '((hard_tabs . t) (skip_children . nil)))
  (remove-hook 'rustic-mode-hook 'flycheck-mode)
  (add-hook 'rustic-mode-hook 'merds/rustic-mode-on))

(custom-set-faces
 '(rustic-compilation-column ((t (:inherit compilation-column-number))))
 '(rustic-compilation-line ((t (:foreground "LimeGreen")))))

;; TypeScript

(defun merds/setup-tide-mode ()
  "Função de Configuração do Typescript."
  (interactive)
  (tide-setup)
  (flycheck-mode 1)
  (setq flycheck-check-syntax-automatically '(save mode-enable))
  (eldoc-mode 1)
  (lsp-mode 1)
  (tide-hl-identifier-mode 1)
  (company-mode 1))
  
(use-package tide
  :ensure t
  :hook (tide-mode . merds/setup-tide-mode))

(setq company-tooltip-align-annotations t)
(add-hook 'before-save-hook 'tide-format-before-save)

(use-package web-mode)
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-hook 'web-mode-hook (lambda ()
                           (when (string-equal "tsx"
                                               (file-name-extension buffer-file-name))
                             (merds/setup-tide-mode))))
(flycheck-add-mode 'typescript-tslint 'web-mode)

;; PYTHON_MODE(Lang Python)

(use-package python
  :mode ("\\.py" . python-mode)
  :config
  (use-package elpy
    :init
    (add-to-list 'auto-mode-alist '("\\.py$" . python-mode))
    :config
    (setq elpy-rpc-backend "jedi")
    (add-hook 'python-mode-hook 'py-autopep8-enable-on-save)
    flycheck-python-flake8-executable "/usr/local/bin/flake8"
    :bind (:map elpy-mode-map)
        ("M-." . elpy-goto-definition)
        ("M-," . pop-tag-mark))
  (elpy-enable))

(use-package pip-requirements
  :config
  (add-hook 'pip-requirements-mode-hook #'pip-requirements-auto-complete-setup))

(setq flycheck-python-pylint-executable "pylint")

(use-package lsp-pyright
  :config
  (setq lsp-clients-python-command "pyright")
  :hook (python-mode . (lambda ()
                         (require 'lsp-pyright)
                         (lsp))))

;; Scheme - Racket-mode

(use-package racket-mode
  :ensure t
  :init
  :config
  (require 'lsp-mode)
  (require 'lsp-racket)
  (add-hook 'racket-mode-hook
            (lambda ()
              define-key racket-mode-map (kbd "<f5>") 'racket-run))
  (add-hook 'racket-mode-hook #'lsp-racket-enable))

(use-package geiser
  :config
  (setq geiser-default-implementation 'racket))


;;; Common Lisp and Clojure

;; Slime (Common Lisp)

(use-package slime
  :ensure t)

(use-package slime-company
  :after (slime company)
  :config (setq slime-company-completion 'fuzzy
		slime-company-after-completion 'slime-company-just-once-space))

;; (add-to-list 'load-path "~/quicklisp/slime/")
;; (setq inferior-lisp-program "/usr/bin/sbcl")
(setq inferior-lisp-program "sbcl")

(add-hook 'lisp-mode-hook (lambda () (slime-mode t)))
(add-hook 'inferior-lisp-mode-hook (lambda () (inferior-slime-mode t)))
(add-hook 'slime-load-hook
	  (lambda ()
	    (define-key slime-prefix-map (kbd "M-h") 'slime-documentation-lookup)))

(require 'slime)
(slime-setup '(slime-fancy slime-company))

(add-hook 'lisp-mode-hook #'prettify-symbols-mode)

;; Clojure (Cider) 
(use-package cider
  :ensure t
  :hook((cider-repl-mode . company-mode)
	(cider-mode . company-mode)
	(cider-repl-mode . cider-company-enable-fuzzy-completion)
	(ciderl-mode . cider-company-enable-fuzzy-completion))
  :config
  (setq cider-prompt-for-symbol t
        cider-jdk-src-paths '("/usr/bin/java"
                              "/usr/local/bin/clojure")
        company-idle-delay nil)
  (setq cider-repl-history-file ".cider-repl-history"  ;; not squiggly-related, but I like it
        nrepl-log-messages t)                          ;; not necessary, but useful for trouble-shooting
  (flycheck-clojure-setup))

;; Clojure-mode

(use-package clojure-mode
  :ensure t
  :hook((clojure-mode . lsp-mode)
        (clojure-mode . cider-mode)
	(clojurescript-mode . lsp-mode)
	(clojure-mode . aggressive-indent-mode)
	(clojurescript-mode . aggressive-indent-mode))
  :config
  (setq lsp-clojure-custom-server-command
	'("bash" "-c" "/usr/local/bin/clojure-lsp"))
  (setq lsp-lens-enable t)
  (setq lsp-enable-completion-at-point nil))

(add-hook 'clojure-mode-hook (lambda () (setq-local comment-column 0)))


;;Flycheck

(use-package flycheck-clojure
  :defer t
  :commands (flycheck-clojure-setup)               ;; autoload
  :config
  (eval-after-load 'flycheck
    '(setq flycheck-display-errors-function #'flycheck-pos-tip-error-messages))
  (add-hook 'after-init-hook #'global-flycheck-mode))

(use-package flycheck-pos-tip
  :ensure t
  :after flycheck)

;; Extra Packages 
(use-package aggressive-indent
  :ensure t)

(add-hook 'emacs-lisp-mode-hook #'aggressive-indent-mode)
(add-hook 'css-mode-hook #'aggressive-indent-mode)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;Terminal

(defun merds/conf-eshell ()
  "Configure eshell"

  (add-hook 'eshell-pre-command-hook 'eshell-save-some-history)
  (add-to-list 'eshell-output-filter-functions 'eshell-truncate-buffer)
  (setq eshell-history-size 10000
	eshell-buffer-maximum-lines 10000
	eshell-hist-ignoredups t
	eshell-scroll-to-bottom-on-output t
        hl-line-mode -1
        display-line-numbers-mode -1))

(use-package eshell
  :hook (eshell-first-time-mode . merds/conf-eshell))

(use-package eshell-git-prompt
  :config
  (eshell-git-prompt-use-theme 'git-radar))

(with-eval-after-load 'esh-opt
  (setq eshell-destroy-buffer-when-process-dies t)
  (setq eshell-visual-commands '("pomodoro"
				 "zsh"
				 "htop")))

;; (use-package vterm
;;   :commands vterm
;;   :load-path "$HOME/data/emacs-libvterm"
;;   :config
;;   (setq term-prompt-regexp "^[^#$%>\n]*[#$%>] *")
;;   (setq vterm-shell "bash")
;;   (setq vterm-max-scrollback 10000)
;;   (defun turn-off-chrome ()
;;     (hl-line-mode -1)
;;     (display-line-numbers-mode -1))
;;   :hook (vterm-mode . turn-off-chrome))

;; (add-hook 'vterm-mode-hook
;;           (lambda ()
;;             (set (make-local-variable 'buffer-face-mode-face) 'fixed-pitch)
;;             (buffer-face-mode t)))

;; (use-package vterm-toggle
;;   :custom
;;   (vterm-toggle-fullscreen-p nil "Open a vterm in another windows")
;;   (vterm-toggle-scope 'projectile)
;;   :bind
;;   (("C-c t" . #'vterm-toggle)
;;    :map vterm-mode-map
;;    ("s-t" . #'vterm)))

(provide 'init);;;

