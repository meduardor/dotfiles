#!/usr/bin/env bash


declare -A LABELS
declare -A COMMANDS

###
# List of defined 'bangs'

# launch programs
COMMANDS["apps"]="rofi -show run"
LABELS["apps"]=""

# open clipboard
COMMANDS["clipboard"]="~/.scripts/green"
LABELS["clipboard"]=""

# search local files
COMMANDS["locate"]="~/.scripts/rofi-files"
LABELS["locate"]=""

# open custom web searches
COMMANDS["websearch"]="~/.scripts/rofi-surfraw-websearch.sh"
LABELS["websearch"]=""

# Open music
COMMANDS["music"]="$HOME/.scripts/music"
LABELS["music"]=""

# Generate menu
##
function print_menu()
{
    for key in ${!LABELS[@]}
    do
  echo "$key    ${LABELS}"
     #   echo "$key    ${LABELS[$key]}"
     # my top version just shows the first field in labels row, not two words side by side
    done
}
##
# Show rofi.
##
function start()
{
    # print_menu | rofi -dmenu -p "?=>" 
    print_menu | sort | rofi -dmenu -mesg ">>> launch your collection of rofi scripts" -i -p "rofi-bangs: "

}


# Run it
value="$(start)"

# Split input.
# grab upto first space.
choice=${value%%\ *}
# graph remainder, minus space.
input=${value:$((${#choice}+1))}

##
# Cancelled? bail out
##
if test -z ${choice}
then
    exit
fi

# check if choice exists
if test ${COMMANDS[$choice]+isset}
then
    # Execute the choice
    eval echo "Executing: ${COMMANDS[$choice]}"
    eval ${COMMANDS[$choice]}
else
 eval  $choice | rofi
 # prefer my above so I can use this same script to also launch apps like geany or leafpad etc (DK) 
 #   echo "Unknown command: ${choice}" | rofi -dmenu -p "error"
fi
