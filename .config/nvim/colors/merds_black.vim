" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" Maintainer:	Marco Eduardo <meduardo.r@protomail.com>
" Last Change:	24 May 2019

set background=light
highlight clear
if exists("syntax_on")
  syntax reset
endif
set t_Co=256
let g:colors_name = "merds_black"
let g:terminal_color_0 = '#ffffec'

hi Comment       ctermfg=22                       
hi Constant      ctermfg=4                       cterm=none   
hi Identifier    ctermfg=136                     cterm=none    
hi Statement     ctermfg=124                     cterm=none    
hi PreProc       ctermfg=34                  
hi Function      ctermfg=178    ctermbg=NONE     cterm=bold
hi String        ctermfg=31     ctermbg=NONE     cterm=bold
hi Keyword       ctermfg=63     ctermbg=NONE     cterm=none
hi Include       ctermfg=22     ctermbg=NONE     cterm=italic
hi Delimiter     ctermfg=100    ctermbg=NONE     cterm=bold
hi Type          ctermfg=33                   
hi Special       ctermfg=36                   
hi Error         ctermbg=9                    
hi Todo          ctermfg=250     ctermbg=0        cterm=bold	
hi Directory     ctermfg=2                    
hi StatusLine    ctermfg=15      ctermbg=16       cterm=bold	
hi Normal        ctermfg=7       ctermbg=0            
hi Search        ctermbg=3                    
hi Operator      ctermfg=9                    
hi Number        ctermfg=13                   
hi Cursor        ctermfg=16      ctermbg=15        cterm=bold
hi CursorLine    ctermfg=NONE    ctermbg=234       cterm=NONE
hi CursorColumn  ctermfg=NONE    ctermbg=234       cterm=NONE
hi StatusLineNC  ctermfg=234     ctermbg=NONE      cterm=bold
hi LineNr        ctermfg=15      ctermbg=16
hi Title         ctermfg=4      ctermbg=NONE     cterm=bold
hi SpellBad      ctermfg=36     ctermbg=NONE     cterm=underline
hi SpellCap      ctermfg=130    ctermbg=NONE     cterm=underline
hi SpellLocal    ctermfg=4      ctermbg=NONE     cterm=underline
hi SpellRare     ctermfg=5      ctermbg=NONE     cterm=underline
hi DiffChange    ctermfg=NONE   ctermbg=254      cterm=NONE
hi DiffAdd       ctermfg=NONE   ctermbg=194      cterm=NONE
hi DiffText      ctermfg=NONE   ctermbg=230      cterm=NONE
hi DiffDelete    ctermfg=167    ctermbg=224      cterm=NONE

syn match    customHeader1     "^# "
syn match    customHeader2     "^## "
syn match    customHeader3     "^### "
syn match    customHeader4     "^#### "
syn match    customHeader5     "^##### "

highlight customHeader1 ctermfg=34
highlight customHeader2 ctermfg=32
highlight customHeader3 ctermfg=127
highlight customHeader4 ctermfg=45
highlight customHeader5 ctermfg=220


