#!/bin/bash

# Terminate already running bar instances
killall -q polybar
# If all your bars have ipc enabled, you can also use 
# polybar-msg cmd quit

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done
# polybar -c ~/.config/polybar/config main &

 polybar main &

 my_laptop_external_display=$(xrandr --query | grep 'HDMI-1')
 if [[ $my_laptop_external_display = *connected* ]]; then
     polybar external &
 fi
